# 指定された検索キーワードでヒットする最新15件のツイートに含まれる名詞の頻出回数を降順に出力するプログラム

* 「spark-submit pytwittermecab.py」で起動

* 環境
    * python 2.7.12
    * Spark 1.6.0
    * MeCab 0.996
    * requests-oauthlib (Python用のOAuth認証ライブラリ)

* 素材
    * https://bitbucket.org/shotaws/hellopytwitter/src
    * https://bitbucket.org/shotaws/hellopymecab/src
   
* 備考
    * 今回の検索キーワードは「#aniaca」

* 取得されたツイート
    * https://bitbucket.org/shotaws/twitterminingwithsparkmecab/src の tweet.txt

* 出力結果
    * https://bitbucket.org/shotaws/twitterminingwithsparkmecab/src の
 result.txt